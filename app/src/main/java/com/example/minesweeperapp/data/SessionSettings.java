package com.example.minesweeperapp.data;

import android.graphics.Bitmap;

public class SessionSettings {
    private static Bitmap background;

    public static Bitmap getBackground() {
        return background;
    }

    public static void setBackground(Bitmap background) {
        SessionSettings.background = background;
    }
}
