package com.example.minesweeperapp.data;

import androidx.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class Record {

    @SerializedName("name")
    private String name;
    @SerializedName("time")
    private int time;

    public String getName() {
        return name;
    }

    public int getTime() {
        return time;
    }
}
