package com.example.minesweeperapp.mvp;

import com.example.minesweeperapp.data.Record;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface RecordsPresenterViewInterface extends MvpView {
    @SingleState
    void fillRecords(List<Record> forms);
}
