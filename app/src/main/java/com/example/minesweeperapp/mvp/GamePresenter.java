package com.example.minesweeperapp.mvp;

import android.os.Handler;
import android.os.Looper;

import com.example.minesweeperapp.game.MinesweeperView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class GamePresenter extends MvpPresenter<GamePresenterViewInterface> {
    private int mTime = 300;
    private MinesweeperView game;
    private boolean running = false;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    Handler handler = new Handler(Looper.getMainLooper());

    @Override
    protected void onFirstViewAttach() {
        startTimer();
    }

    public void setupGame(MinesweeperView game) {
        this.game = game;
        game.setCallback(new MinesweeperView.GameCallback() {
            @Override
            public void onLose() {
                if(running) {
                    getViewState().gameLost();
                    running = false;
                }
            }

            @Override
            public void onWin() {
                if(running) {
                    getViewState().gameWon(mTime);
                    running = false;
                }
            }
        });
    }

    public void setMode(boolean mode) {game.setMode(mode);}

    public void newGame() {
        game.newField();
        mTime = 300;
        startTimer();
    }

    private void startTimer() {
        if(!running) {
            running = true;
            executor.execute(() -> {
                while(mTime > 0 && running) {
                    try {
                        handler.post(this::timeUp);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private void timeUp() {
        mTime--;
        String time;
        int s = mTime % 60;
        if(s < 10) {
            time = mTime / 60 + ":0" + s;
        } else time = mTime / 60 + ":" + s;
        getViewState().setTimer(time);
        if(mTime <= 0) getViewState().gameLost();
    }
}
