package com.example.minesweeperapp.mvp;

import androidx.annotation.NonNull;

import com.example.minesweeperapp.data.Record;
import com.example.minesweeperapp.util.MyServerClient;
import com.example.minesweeperapp.util.ServerClient;

import java.util.Collections;
import java.util.List;

import moxy.InjectViewState;
import moxy.MvpPresenter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class RecordsPresenter extends MvpPresenter<RecordsPresenterViewInterface> {

    @Override
    protected void onFirstViewAttach() {
        ServerClient serverClient = MyServerClient.createService(ServerClient.class);
        Call<List<Record>> call = serverClient.getRecords();
        call.enqueue(new Callback<List<Record>>() {
            @Override
            public void onResponse(@NonNull Call<List<Record>> call, @NonNull Response<List<Record>> response) {
                sort(response.body());
                getViewState().fillRecords(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Record>> call, @NonNull Throwable t) {

            }
        });
    }

    private void sort(List<Record> array) {
        Collections.sort(array, (o1, o2) -> o2.getTime() - o1.getTime());
    }
}
