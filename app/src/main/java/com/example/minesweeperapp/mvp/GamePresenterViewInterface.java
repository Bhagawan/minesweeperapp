package com.example.minesweeperapp.mvp;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.SingleState;

public interface GamePresenterViewInterface extends MvpView {

    @SingleState
    void setTimer(String time);

    @SingleState
    void gameLost();

    @SingleState
    void gameWon(int time);

}
