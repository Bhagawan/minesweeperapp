package com.example.minesweeperapp.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.VectorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;

import com.example.minesweeperapp.R;
import com.example.minesweeperapp.util.Util;

import java.util.ArrayList;
import java.util.Arrays;

public class MinesweeperView extends View {
    private static final int MINE_AMOUNT = 15;
    private static final int SIZE_X = 10;
    private static final int SIZE_Y = 10;
    public static final boolean MODE_PRESS = true;
    public static final boolean MODE_FLAG = false;

    private float mHeight;
    private float mWidth;
    private float mViewPaddingLeft;
    private float mViewPaddingTop;
    private float mSquareSize = 0;
    private boolean[][] pressed;
    private ArrayList<Point> mFlags = new ArrayList<>();
    private boolean mModePress = true;

    private Bitmap mMineBitmap;
    private Bitmap mFlagBitmap;
    private Field mField;

    private GameCallback mCallback;

    private Context context;

    public MinesweeperView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onDraw(Canvas c) {
        if(mSquareSize > 0 ){
            drawField(c);
        }
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        if(getWidth() > 0) initialise();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {

            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_UP) {
            click(event.getX(), event.getY());
            invalidate();
            return true;
        }
        return super.onTouchEvent(event);
    }

    public void setCallback(GameCallback callback) {this.mCallback = callback;}

    public void setMode(boolean mode) {this.mModePress = mode;}

    public void newField() {
        mField = new Field(SIZE_X, SIZE_Y, MINE_AMOUNT);
        mFlags.clear();
        for(boolean[] b : pressed) {
            Arrays.fill(b, false);
        }
        invalidate();
    }

    private void initialise() {
        mViewPaddingLeft = getPaddingLeft();
        mViewPaddingTop = getPaddingTop();
        mWidth = getWidth() - getPaddingLeft() - getPaddingEnd();
        mHeight = getHeight() - getPaddingTop() - getPaddingBottom();

        mField = new Field(SIZE_X, SIZE_Y, MINE_AMOUNT);
        pressed = new boolean[mField.getWidth()][mField.getHeight()];
        for(boolean[] b : pressed) {
            Arrays.fill(b, false);
        }

        if(mWidth < mHeight) mSquareSize = mWidth / mField.getWidth();
        else mSquareSize = mHeight / mField.getHeight();

        mMineBitmap = Util.svgToBitmap( ContextCompat.getDrawable(context, R.drawable.ic_bomb)
                , (int)mSquareSize - 5, (int)mSquareSize - 5);

        mFlagBitmap = Util.svgToBitmap( ContextCompat.getDrawable(context, R.drawable.ic_redflag)
                , (int)(mSquareSize - 10 - (mSquareSize / 10)), (int)(mSquareSize - 10 - (mSquareSize / 10)));
    }

    private void drawField(Canvas c) {
        for(int x = 0; x < mField.getWidth(); x++) {
            for(int y = 0; y < mField.getHeight(); y++) {
                if(pressed[x][y]) {
                    Paint p = new Paint();
                    p.setColor(Color.parseColor("#807C7C"));
                    p.setStyle(Paint.Style.FILL);
                    c.drawRect(x * mSquareSize + mViewPaddingLeft,
                            y * mSquareSize + mViewPaddingTop,
                            (x * mSquareSize) + mSquareSize + mViewPaddingLeft,
                            (y * mSquareSize) + mSquareSize + mViewPaddingTop, p);

                    p.setColor(Color.parseColor("#434242"));
                    p.setStyle(Paint.Style.STROKE);
                    c.drawRect(x * mSquareSize + mViewPaddingLeft,
                            y * mSquareSize + mViewPaddingTop,
                            (x * mSquareSize) + mSquareSize + mViewPaddingLeft,
                            (y * mSquareSize) + mSquareSize + mViewPaddingTop, p);
                    p.setColor(Color.BLACK);
                    p.setTextSize(mSquareSize / 2);
                    p.setTextAlign(Paint.Align.CENTER);
                    if(mField.get(x, y) == -1) {
                        c.drawBitmap(mMineBitmap, (float) ((x * mSquareSize) + 2.5) + mViewPaddingLeft,
                                (float) ((y * mSquareSize) + 2.5) + mViewPaddingTop, p);
                    } else if(mField.get(x, y) > 0){
                        c.drawText(String.valueOf(mField.get(x, y)), (x * mSquareSize) + (mSquareSize / 2) + mViewPaddingLeft,
                               (y * mSquareSize) + (mSquareSize / 3 * 2) + mViewPaddingTop, p);
                    }
                } else {
                    Paint p = new Paint();
                    p.setColor(Color.parseColor("#807C7C"));
                    p.setStyle(Paint.Style.FILL);
                    c.drawRect(x * mSquareSize + mViewPaddingLeft,
                            y * mSquareSize + mViewPaddingTop,
                            (x * mSquareSize) + mSquareSize + mViewPaddingLeft,
                            (y * mSquareSize) + mSquareSize + mViewPaddingTop, p);

                    p.setColor(Color.parseColor("#434242"));
                    p.setStyle(Paint.Style.STROKE);
                    c.drawRect(x * mSquareSize + mViewPaddingLeft,
                            y * mSquareSize + mViewPaddingTop,
                            (x * mSquareSize) + mSquareSize + mViewPaddingLeft,
                            (y * mSquareSize) + mSquareSize + mViewPaddingTop, p);

                    float w = mSquareSize / 10;
                    Path path = new Path();
                    path.moveTo(x * mSquareSize + mViewPaddingLeft, y * mSquareSize + mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mSquareSize + mViewPaddingLeft, y * mSquareSize + mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mSquareSize + mViewPaddingLeft - w, y * mSquareSize + mViewPaddingTop + w);
                    path.lineTo(x * mSquareSize + mViewPaddingLeft + w, y * mSquareSize + mViewPaddingTop + w);
                    path.lineTo(x * mSquareSize + mViewPaddingLeft + w, y * mSquareSize + mSquareSize - mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mViewPaddingLeft, y * mSquareSize + mSquareSize + mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mViewPaddingLeft, y * mSquareSize + mViewPaddingTop);
                    path.close();
                    p.setStyle(Paint.Style.FILL);
                    p.setColor(Color.parseColor("#C6C4C4"));
                    c.drawPath(path, p);

                    path.reset();
                    path.moveTo(x * mSquareSize + mSquareSize + mViewPaddingLeft, y * mSquareSize + mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mSquareSize + mViewPaddingLeft, y * mSquareSize + mSquareSize + mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mViewPaddingLeft, y * mSquareSize + mSquareSize + mViewPaddingTop);
                    path.lineTo(x * mSquareSize + mViewPaddingLeft + w, y * mSquareSize + mSquareSize + mViewPaddingTop - w);
                    path.lineTo(x * mSquareSize + mSquareSize + mViewPaddingLeft - w, y * mSquareSize + mSquareSize + mViewPaddingTop - w);
                    path.lineTo(x * mSquareSize + mSquareSize + mViewPaddingLeft - w, y * mSquareSize + mViewPaddingTop + w);
                    path.close();
                    p.setColor(Color.parseColor("#434242"));
                    c.drawPath(path, p);
                    if(mFlags.contains(new Point(x, y))) c.drawBitmap(mFlagBitmap,(float) ((x * mSquareSize)  + w) + mViewPaddingLeft,
                            (float) ((y * mSquareSize)  + w) + mViewPaddingTop, p);
                }
            }
        }
    }

    private void click(float x, float y) {
        int fieldX = (int)((x - mViewPaddingLeft) / mSquareSize);
        int fieldY = (int)((y - mViewPaddingTop) / mSquareSize);
        if(mModePress) {
            if(mField.get(fieldX, fieldY) == -1) {
                for(int i = 0; i < mField.getWidth(); i++) {
                    for(int j = 0; j < mField.getHeight(); j++) {
                        if(mField.get(i, j) == -1 && !mFlags.contains(new Point(i, j))) pressed[i][j] = true;
                    }
                }
                if(mCallback != null) mCallback.onLose();
            } else {
                openField(fieldX, fieldY);
                winCheck();
            }
        } else {
            Point p = new Point(fieldX, fieldY);
            if(fieldX >= 0 && fieldX < mField.getWidth() && fieldY >= 0 && fieldY < mField.getHeight())
                if(mFlags.contains(p)) mFlags.remove(p);
                else mFlags.add(p);
        }
    }

    private void openField(int x, int y) {
        if(x >= 0 && x < mField.getWidth() && y >= 0 && y < mField.getHeight()) {
            if(mField.get(x, y) >= 0) pressed[x][y] = true;
            if(mField.get(x, y) == 0) {
                if(x - 1 >= 0) if(!pressed[x - 1][y]) openField(x - 1, y);
                if(x - 1 >= 0 && y - 1 >=0) if(!pressed[x - 1][y - 1]) openField(x - 1, y - 1);
                if(x + 1 < mField.getWidth()) if(!pressed[x + 1][y]) openField(x + 1, y);
                if(x + 1 < mField.getWidth() && y - 1 >= 0) if(!pressed[x + 1][y - 1]) openField(x + 1, y - 1);
                if(y - 1 >= 0) if(!pressed[x][y - 1]) openField(x, y - 1);
                if(y + 1 < mField.getHeight()) if(!pressed[x][y + 1]) openField(x, y + 1);
                if(y + 1 < mField.getHeight() && x - 1 >= 0) if(!pressed[x - 1][y + 1]) openField(x - 1, y + 1);
                if(y + 1 < mField.getHeight() && x + 1 < mField.getWidth()) if(!pressed[x + 1][y + 1]) openField(x + 1, y + 1);
            }
        }
    }

    private void winCheck() {
        boolean win = true;
        int x = 0;
        int y;
        while(win && x < mField.getWidth()) {
            y = 0;
            while(win && y < mField.getHeight()) {
                if(!pressed[x][y] && mField.get(x, y) != -1) win = false;
                y++;
            }
            x++;
        }
        if(win && mCallback != null) mCallback.onWin();
    }

    public interface GameCallback {
        void onLose();
        void onWin();
    }

}
