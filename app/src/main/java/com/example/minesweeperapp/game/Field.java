package com.example.minesweeperapp.game;

import java.util.Random;

public class Field {
    private int mine_amount;
    private int size_x;
    private int size_y;

    private int[][] field;

    public Field(int size_x, int size_y, int mine_amount) {
        this.size_x = size_x;
        this.size_y = size_y;
        this.mine_amount = mine_amount;

        field = new int[size_x][size_y];
        fill();
    }

    public int getWidth() {return size_x;}

    public int getHeight() {return size_y;}

    public int get(int x, int y) {
        return field[x][y];
    }

    private void fill() {
        for(int x = 0; x < size_x; x++) {
            for(int y = 0; y < size_y; y++) {
                field[x][y] = 0;
            }
        }

        for(int i = 0; i < mine_amount; i++) {
            Random r = new Random();
            int x;
            int y;

            do {
                x = r.nextInt(size_x);
                y = r.nextInt(size_y);
            } while(field[x][y] == -1);
            field[x][y] = -1;

            for(int dx = -1; dx < 2; dx++) {
                for (int dy = -1; dy < 2; dy++) {
                    if((x + dx) >= 0 && (x + dx) < size_x && (y + dy) >= 0 && (y + dy) < size_y) {
                        if(field[x + dx][y + dy] != -1) field[x + dx][y + dy]++;
                    }
                }
            }
        }
    }

}
