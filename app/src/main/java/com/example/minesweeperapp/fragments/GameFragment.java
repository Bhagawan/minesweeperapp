package com.example.minesweeperapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.minesweeperapp.R;
import com.example.minesweeperapp.data.SessionSettings;
import com.example.minesweeperapp.game.MinesweeperView;
import com.example.minesweeperapp.mvp.GamePresenter;
import com.example.minesweeperapp.mvp.GamePresenterViewInterface;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.Timer;
import java.util.TimerTask;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class GameFragment extends MvpAppCompatFragment implements GamePresenterViewInterface {
    private View mView;
    private Target target;

    @InjectPresenter
    GamePresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_game, container, false);
        setBackground();

        MinesweeperView game = mView.findViewById(R.id.minesweeperView);
        mPresenter.setupGame(game);

        ImageButton backButton = mView.findViewById(R.id.button_game_back);
        backButton.setOnClickListener(v -> getParentFragmentManager().popBackStack());

        ImageButton newGameButton = mView.findViewById(R.id.button_game_new);
        newGameButton.setOnClickListener(v -> mPresenter.newGame());

        ImageButton tapButton = mView.findViewById(R.id.button_game_press);
        tapButton.setSelected(true);
        ImageButton flagButton = mView.findViewById(R.id.button_game_flag);
        tapButton.setOnClickListener(v -> {
            flagButton.setSelected(false);
            tapButton.setSelected(true);
            mPresenter.setMode(MinesweeperView.MODE_PRESS);
        });
        flagButton.setOnClickListener(v -> {
            tapButton.setSelected(false);
            flagButton.setSelected(true);
            mPresenter.setMode(MinesweeperView.MODE_FLAG);
        });

        return mView;
    }

    @Override
    public void setTimer(String time) {
        TextView timer = mView.findViewById(R.id.textView_game_time);
        timer.setText(time);
    }

    @Override
    public void gameLost() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),R.style.transparentAlert);

        View alertView = getLayoutInflater().inflate(R.layout.alert_lose, null, false);
        builder.setView(alertView);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Timer time  = new Timer();
        time.schedule(new TimerTask() {
            @Override
            public void run() {
                alertDialog.dismiss();
                getParentFragmentManager().popBackStack();
            }
        }, 2000);
    }

    @Override
    public void gameWon(int time) {
        Intent intent = new Intent(getContext(), SaveActivity.class);
        intent.putExtra("time", time);
        startActivity(intent);
    }

    private void setBackground() {
        ConstraintLayout back = mView.findViewById(R.id.layout_game_main);
        if(SessionSettings.getBackground() != null) back.setBackground(null);
        else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    SessionSettings.setBackground(bitmap);
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://195.201.125.8/MinesweeperApp/background.png").into(target);
        }
    }
}