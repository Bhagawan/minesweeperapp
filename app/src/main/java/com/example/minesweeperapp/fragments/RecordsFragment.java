package com.example.minesweeperapp.fragments;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.minesweeperapp.R;
import com.example.minesweeperapp.data.Record;
import com.example.minesweeperapp.data.SessionSettings;
import com.example.minesweeperapp.mvp.RecordsPresenter;
import com.example.minesweeperapp.mvp.RecordsPresenterViewInterface;
import com.example.minesweeperapp.util.RecordsAdapter;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;

public class RecordsFragment extends MvpAppCompatFragment implements RecordsPresenterViewInterface {
    private View mView;

    @InjectPresenter
    RecordsPresenter mPresenter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                getParentFragmentManager().popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_records, container, false);
        setBackground();

        ImageButton backButton = mView.findViewById(R.id.btn_records_back);
        backButton.setOnClickListener(v -> getParentFragmentManager().popBackStack());
        return mView;
    }

    @Override
    public void fillRecords(List<Record> records) {
        RecyclerView recyclerView = mView.findViewById(R.id.recycler_records);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new RecordsAdapter(records));
    }

    private void setBackground() {
        ConstraintLayout back = mView.findViewById(R.id.layout_records);
        if(SessionSettings.getBackground() != null) back.setBackground(new BitmapDrawable(getResources(), SessionSettings.getBackground()));
    }
}