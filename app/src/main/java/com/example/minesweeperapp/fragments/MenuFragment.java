package com.example.minesweeperapp.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.minesweeperapp.R;
import com.example.minesweeperapp.data.SessionSettings;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


public class MenuFragment extends Fragment {
    private View mView;
    private Target target;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                requireActivity().finish();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_menu, container, false);
        setBackground();

        FragmentTransaction ft = getParentFragmentManager().beginTransaction();
        Button newGame = mView.findViewById(R.id.btn_menu_game);
        newGame.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new GameFragment())
                .addToBackStack(null).commit());

        Button records = mView.findViewById(R.id.btn_menu_records);
        records.setOnClickListener(v -> ft.replace(R.id.fragmentContainerView, new RecordsFragment())
                .addToBackStack(null).commit());
        return mView;
    }

    private void setBackground() {
        ConstraintLayout back = mView.findViewById(R.id.layout_menu_main);
        if(SessionSettings.getBackground() != null) back.setBackground(null);
        if(SessionSettings.getBackground() != null) back.setBackground(null);
        else {
            target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    SessionSettings.setBackground(bitmap);
                    back.setBackground(new BitmapDrawable(getResources(), bitmap));
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            };
            Picasso.get().load("http://195.201.125.8/MinesweeperApp/background.png").into(target);
        }
    }

}