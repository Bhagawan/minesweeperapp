package com.example.minesweeperapp.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public class Util {

    public static Bitmap svgToBitmap(Drawable vectorDrawable, int x, int y) {
        Bitmap bitmap = Bitmap.createBitmap(x,
                y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);

        return bitmap;
    }
}
