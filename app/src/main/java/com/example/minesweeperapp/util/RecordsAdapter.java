package com.example.minesweeperapp.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.minesweeperapp.R;
import com.example.minesweeperapp.data.Record;

import java.util.List;

public class RecordsAdapter extends RecyclerView.Adapter<RecordsAdapter.ViewHolder> {
    private List<Record> records;

    public RecordsAdapter(List<Record> data) {
        this.records = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView time;
        private TextView place;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            place = itemView.findViewById(R.id.item_record_place);
            name = itemView.findViewById(R.id.item_record_name);
            time = itemView.findViewById(R.id.item_record_time);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_records, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Record record = records.get(position);
        holder.place.setText(String.valueOf(position + 1));
        holder.name.setText(record.getName());
        int time = record.getTime();
        String fTime;
        if(time % 60 < 10) fTime = time / 60 + ":0" + time % 60;
        else fTime = time / 60 + ":" + time % 60;
        holder.time.setText(fTime);
    }

    @Override
    public int getItemCount() {
        if(records == null) return 0;
        else return records.size();
    }
}