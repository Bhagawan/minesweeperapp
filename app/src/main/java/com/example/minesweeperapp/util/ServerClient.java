package com.example.minesweeperapp.util;

import com.example.minesweeperapp.data.Record;
import com.example.minesweeperapp.data.SplashResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServerClient {

    @FormUrlEncoded
    @POST("MinesweeperApp/addScore.php")
    Call<ResponseBody> sendRecord( @Field("time") int time, @Field("name") String name);

    @GET("MinesweeperApp/score.json")
    Call<List<Record>> getRecords();

    @FormUrlEncoded
    @POST("MinesweeperApp/splash.php")
    Call<SplashResponse> getSplash(@Field("locale") String locale);

}
